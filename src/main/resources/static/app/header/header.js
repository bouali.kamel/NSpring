define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dojo/text!app/header/header.html",
        "dojo/i18n!app/header/nls/local",
        "js/user",
        "dojo/domReady!"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template,
        i18n,
        user
    ) {
        return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: template,
            i18n: i18n,
            startup: function () {

                var search = document.createElement("div");
                $(this.domNode).find('.searchGeo').append(search);

             
                $(this.domNode).find('#toggleBaseGallery').click(function (e) {
                    e.preventDefault();
                    $(".sideBar").toggleClass("hidden");
                });

                $(this.domNode).find('#changeLanguage').click(function (e) {
                    e.preventDefault();

                    localStorage.setItem('locale', $(this).attr("data-language"));
                    location.reload();


                });

                $(this.domNode).find('#userSession').html(user.prenom + " " + user.nom);
                
                $(this.role).html(user.role);
                	                
                $(this.domNode).find('#fullScreen').click($.proxy(function (e) {
                    e.preventDefault();

                    var elem = document.body; 
                    this.toggleFullscreen(elem);
                    
                }, this));
                

                
                
                


            },
            toggleFullscreen: function (elem) {
                elem = elem || document.documentElement;
                if (!document.fullscreenElement && !document.mozFullScreenElement &&
                    !document.webkitFullscreenElement && !document.msFullscreenElement) {
                    if (elem.requestFullscreen) {
                        elem.requestFullscreen();
                    } else if (elem.msRequestFullscreen) {
                        elem.msRequestFullscreen();
                    } else if (elem.mozRequestFullScreen) {
                        elem.mozRequestFullScreen();
                    } else if (elem.webkitRequestFullscreen) {
                        elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                } else {
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if (document.msExitFullscreen) {
                        document.msExitFullscreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    }
                }
            }
        });
    });