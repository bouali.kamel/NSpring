define([
    "js/initWidgets",
    "config/mapConfig",
    "config/layerConfig",
    "config/searchConfig",
    "js/loader",
    "esri/dijit/Search",
    "js/events",
    "js/user",
    "dojo/domReady!"
], function (initWidgets, mapConfig, layerConfig, searchConfig, loader, Search, events,user) {

    return {
        map: null,
        startup: function () {

            this.initLanguage();
           
            user.initUser($.proxy(function(){
            	this.initMap();
            },this)); 
            console.log(user.token)
            console.log(user.ipArcgis)
         
            
            $(window).on('load', function () {
                loader.windowLoaded = true;
                if (loader.appLoaded) {
                    loader.onLoad();
                }
            });

        },
        initMap: function () {
            this.map = mapConfig.map;
            this.map.setVisibility(false);

            this.map.on("load", $.proxy(function () {
                this.initLayer();
                initWidgets.startup(this.map);
                this.initSearch();
                events.map = this.map;
                events.initEvent();
            }, this));

        },
        
        
        
        initLayer: function () {
            var layers = layerConfig.layers
            this.map.addLayers(layers);

        },
        initSearch: function () {
            var searchContainer = document.createElement("div");
            $("#main").append(searchContainer);

            var search = new Search({
                map: this.map,
                sources: searchConfig.sources
            }, searchContainer);

            search.startup();
        },
        initLanguage: function () {

            if (localStorage.getItem('locale') == 'ar') {
                $('body').addClass('rightToLeft');

            } else {
                $('body').removeClass('rightToLeft');
            }
        }
    }


});