package elit.sig.config;
import org.json.JSONObject;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class OnStartServer {

  @EventListener(ContextRefreshedEvent.class)
  public static void generateTokenAdmin() throws Exception{
	  String url = "http://" + UserArcgis.IP_ARCGIS_SERVER + ":6080/arcgis/tokens/generateToken";
	String formData = "username=" + UserArcgis.USER_ADMIN + "&password=" + UserArcgis.PASSWORD_ADMIN + "&ip="+ UserArcgis.IP_ARCGIS_SERVER + "&expiration=" + UserArcgis.EXPIRATION + "&f=pjson&client=ip";
		
		String userLiset = HttpURLConnectionSend.sendingPostRequest(url, formData);
		JSONObject userLisetObjet = new JSONObject(userLiset);
		String request = userLisetObjet.getString("token").toString();
		

		
		UserArcgis.TOKEN_ADMIN=request;
  }
}

