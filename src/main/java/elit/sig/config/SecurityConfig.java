package elit.sig.config;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.ldap.userdetails.InetOrgPersonContextMapper;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	
	
	
	@Value("${ldap.userSearchFilter}") private String userSearchFilter; 
	@Value("${ldap.userSearchBase}") private String userSearchBase;
	@Value("${ldap.groupSearchBase}") private String groupSearchBase;
	@Value("${ldap.groupSearchFilter}") private String groupSearchFilter;
	@Value("${ldap.url}") private String url;
	@Value("${ldap.username}") private String managerDn;
	@Value("${ldap.password}") private String managerPassword;


	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
	
		//.expiredUrl("/login?expiredSession");
		
		
		
		http
		.csrf().disable()
//		.requireCsrfProtectionMatcher(new AntPathRequestMatcher("**/login")).and()
		
		
		//---------------------------------------------------AUTHORISATION----------------------------------
		.authorizeRequests()
		.antMatchers("/index","/").authenticated()
		//---------------------------------------------------LOGIN----------------------------------
		.and()
			.formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/")
				.failureHandler((req,res,exp)->{ 
			         String errMsg="";
			         if(exp.getClass().isAssignableFrom(BadCredentialsException.class)){
			            errMsg="Nom d'utilisateur ou mot de passe incorrect";
			         }else if(exp.getClass().isAssignableFrom(SessionAuthenticationException.class)) { 
			        	 errMsg="Votre session est déjà ouverte, veuillez vous déconnecter !";
			        	 }
			         else { 
			        	 errMsg="Erreur inconnue - "+exp.getMessage();
			         }
			         req.getSession().setAttribute("message", errMsg);
			         res.sendRedirect("/"); 
			      })
		
		//---------------------------------------------------LOGOUT----------------------------------
		.and()
			.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessHandler((req,res,auth)->{
			         req.getSession().setAttribute("message", "Déconnexion réussite");
			         res.sendRedirect("/login"); 
			      })
				.deleteCookies("JSESSIONID")
				.invalidateHttpSession(true)
				;
		//--------------------------------------------Gestion des sessions----------------------------------
		http
			.sessionManagement()
//				.invalidSessionUrl("/login")
				.maximumSessions(1)	
				.maxSessionsPreventsLogin(true)
				.sessionRegistry(sessionRegistry());
	}

	
	
	
	
	
	
	@Autowired 
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth
		.ldapAuthentication()
		.userDetailsContextMapper(userContextMapper())
		.userSearchFilter(userSearchFilter)
		.userSearchBase("")
		.groupSearchBase(groupSearchBase)
		.groupSearchFilter(groupSearchFilter)
		.contextSource()
		.url(url) 
		.managerDn(managerDn) 
		.managerPassword(managerPassword);
		
	}
	    @Bean
	    public InetOrgPersonContextMapper userContextMapper() {
	        return new InetOrgPersonContextMapper();
	    }
		@Bean
		public SessionRegistry sessionRegistry() {
		    SessionRegistry sessionRegistry = new SessionRegistryImpl();
		    return sessionRegistry;
		}
		@Bean
		public static ServletListenerRegistrationBean httpSessionEventPublisher() {
		    return new ServletListenerRegistrationBean(new HttpSessionEventPublisher());
		}
}