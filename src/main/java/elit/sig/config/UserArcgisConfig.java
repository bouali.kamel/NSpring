package elit.sig.config;



import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public final class UserArcgisConfig {

	public static String checkUser() {
		return "";
	}

	
	public static Authentication userAuthentication() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication;
	}
	
	
	public static boolean isAutenticate() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication.isAuthenticated();
	}
	
	// search if exist a users
	public static boolean searchUser() throws Exception {

		String url = "http://" + UserArcgis.IP_ARCGIS_SERVER + ":6080/arcgis/admin/security/users/search?token="
				+ UserArcgis.TOKEN_ADMIN;
		String formData = "filter=" + UserArcgisConfig.userAuthentication().getName() + "&f=pjson";
		String userLiset = HttpURLConnectionSend.sendingPostRequest(url, formData);
		JSONObject userLisetObjet = new JSONObject(userLiset);
		JSONArray arr = userLisetObjet.getJSONArray("users");
		String userarcgis = "";
		if (!arr.isNull(0))
			userarcgis = arr.getJSONObject(0).getString("username");
		return (userarcgis.equals(UserArcgisConfig.userAuthentication().getName()));
	}
	
	
	public static String addUser() throws Exception {
		String url = "http://" + UserArcgis.IP_ARCGIS_SERVER + ":6080/arcgis/admin/security/users/add?token="
				+ UserArcgis.TOKEN_ADMIN;
		String formData = "username=" + UserArcgisConfig.userAuthentication().getName() + "&password="
				+ UserArcgis.PASSWORD_CLIENT + "&f=pjson";
		String userLiset = HttpURLConnectionSend.sendingPostRequest(url, formData);
		JSONObject userLisetObjet = new JSONObject(userLiset);
		String request = userLisetObjet.getString("status").toString();
		return request;
	}
	
	
	public static String generateTokenClient(String IP_CILENT) throws Exception {
		String url = "http://" + UserArcgis.IP_ARCGIS_SERVER + ":6080/arcgis/tokens/generateToken";
		String formData = ""
				+ "username=" + UserArcgisConfig.userAuthentication().getName() 
				+ "&password="	+ UserArcgis.PASSWORD_CLIENT 
				+ "&ip=" + IP_CILENT 
				+ "&expiration=" + UserArcgis.EXPIRATION
				+ "&f=pjson&client=ip";
	
		String userLiset = HttpURLConnectionSend.sendingPostRequest(url, formData);
		JSONObject userLisetObjet = new JSONObject(userLiset);
		String request = userLisetObjet.getString("token").toString();
		return request;
	}
	
	
	
}