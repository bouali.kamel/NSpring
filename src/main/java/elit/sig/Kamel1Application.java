package elit.sig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kamel1Application {

	public static void main(String[] args) {
		SpringApplication.run(Kamel1Application.class, args);
	}
}
