<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Spring Security Example</title>
<link href="/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="/libs/jquery/jquery-3.3.1.min.js"></script>
<script src="/libs/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container" style="margin: 50px;border: 1px solid green;">
		<h3>Spring Security Login Example</h3>
		
		
		<c:if test="${not empty sessionScope.message}">
    	<span style="color:red"><c:out value="${sessionScope.message}"/></span>
    	<c:remove var="message" scope="session" />
 		</c:if>
 		
 		
		<form action="/login" method="post">
			<div class="form-group">
				<label for="username">UserName:</label> <input type="text"
					class="form-control" id="username" name="username">
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label> <input type="password"
					class="form-control" id="pwd" name="password">
			</div>

			<button type="submit" class="btn btn-success">Submit</button>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
	</div>
</body>
</html>